# Oppgave 1
# Lag en funksjon skriv(). Det eneste den skal gjøre
# er å skrive ut tallet 42 til terminalen.
# Denne funksjonen kalles ikke for å gjøre noe som
# den skal returnere til oss, men for noe den gjør selv.

def skriv():
    print(42)
    
skriv() # vi kaller den

# Oppgave 2 
# Lag en funksjon skriv_dette(dette). Den skal ta imot
# en parameter, så skal denne skrives ut i terminalen

def skriv_dette(dette): 
    print(dette)
    
# Når vi kaller skriv_dette må vi ha med et argument.
# Det e jo akkurat det definisjonen av skriv_dette har!
skriv_dette('dette') # kalles, med et 'argument'

# Nå skal dere jobbe med et annet aspekt ved funksjoner,
# og det er at vi bevisst kan returnere noe fra dem.
# Dette er det viktig at dere forstår. På forelesning
# ba jeg dere om å regne ut hvor mange tegn det var i
# fornavnet, og så skrive det i chat. Dere returnerte det
# til meg, slik at jeg kunne bruke det videre hvis jeg ville
# Funksjoner som ikke har en 'return <noe>' vil bare
# returnere 'NoneType' elns.
# Den eksisterende funksjonen print er en funksjon som de over
# den gjør en jobb og så returnerer den ikke noe av verdi.
# Det er grunnen til at denne koden feiler (verdi = None)
verdi = print(2+4)
print(verdi)


# Oppgave 3 - returverdi
# Lag funksjonen beregn_areal_av_rektangel(lengde, bredde).
# Funksjonen skal beregne arealet av rektangelet, og så
# returnere arealet.

def beregn_areal_av_rektangel(lengde, bredde):
    areal = lengde * bredde
    return areal # kunne også skrevet: return lengde * bredde

min_lengde = 7
min_bredde = 8
beregnet_areal = beregn_areal_av_rektangel(min_lengde, min_bredde)
print(f'Arealet av et rektangel med lengde {min_lengde} og bredde ' \
      f'{min_lengde} er {beregnet_areal}')
# Mens oppgave 1 og 2 skrev ting ut til terminalvinduet selv,
# så gjør ikke denne det. Derfor må vi skrive resultatet ut
# hvis vi kaller funksjonen direkte.
beregn_areal_av_rektangel(3,4) # 12 skrives aldri ut
print(beregn_areal_av_rektangel(5,6)) # skal skrive ut 30

# oppgave med while OG kall av annen, selvlaget funkjon
# Og mulighet til å bruke break om en vil

# Oppgave 4 - en funksjon kan bruke en annen funksjon!
# Lag en funksjon stort_rektangel(). Denne skal spørre
# brukeren om lengde og bredde til et rektangel. Hvis
# arealet av rektangelet er mindre enn 100, da skal den
# spørre igjen. Hvis areal er lik eller større enn 100,
# da skal den returnere lengde og bredde
# Man returnerer både a og b slik: return a, b
# Og HUSK: du har allerede funksjonen
# beregn_areal_av_rektangel. Kan du bruke denne?

def stort_rektangel():
    
    lengde = 0
    bredde = 0

    while beregn_areal_av_rektangel(lengde, bredde) < 100:
        lengde = int(input('Lengde: '))
        bredde = int(input('Bredde: '))
    return lengde, bredde

#lengde, bredde = stort_rektangel()
#print(f'{lengde}*{bredde} er minst 100.')

# Denne kan skrives på maaaange måter:
def stort_rektangel_alternativ():

    while True:
        lengde = int(input('Lengde: '))
        bredde = int(input('Bredde: '))
        if beregn_areal_av_rektangel(lengde, bredde) > 100:
            return lengde, bredde
print(stort_rektangel_alternativ())

# Så dere noe merkelig med koden over? Jeg brukte variabelnavnene
# lengde og bredde inni stort_rektangel, beregn_areal_av_rektangel
# OG utenfor funksjonene! Vi skal i neste uke se på hva dette er